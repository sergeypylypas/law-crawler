import Bottleneck from "bottleneck";

const limiter = new Bottleneck({
  minTime: 1000,
  maxConcurrent: 1,
});

export const throttle = <T>(fn: (...args: any[]) => Promise<T>) => {
  return limiter.wrap(fn);
};
