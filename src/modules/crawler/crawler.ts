// import { parseHTML } from "./parsers/htmlParser";
// import { parseXML } from "./parsers/xmlParser";
// import { Law, Judgment } from "../database/models";

export enum CrawlerEventType {
  created = "crawler.created",
  started = "crawler.started",
  stopped = "crawler.stopped",

  scraped = "entity.scraped",
}

export interface ICrawler {
  handle: string;
  on(eventType: CrawlerEventType, cb: Function): void;
  start(): Promise<void>;
}
