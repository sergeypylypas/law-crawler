import { ICrawler } from "../crawler";

// https://www.fedlex.admin.ch/en/cc?news_period=last_year&news_pageNb=1&news_order=desc&news_itemsPerPage=10
// https://www.fedlex.admin.ch/elasticsearch/proxy/_search?index=data
export class FedlexLawsCrawler implements ICrawler {
  handle = "fedlex.laws";

  constructor() {}

  async start(): Promise<void> {}

  on() {}
}
