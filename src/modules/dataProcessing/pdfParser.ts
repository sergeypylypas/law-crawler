import pdfParse from "pdf-parse";

export const parsePDF = async (pdfBuffer: Buffer) => {
  const data = await pdfParse(pdfBuffer);
  return data.text;
};
