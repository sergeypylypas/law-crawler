// import { Law, Judgment } from "../database/models";
// import { parsePDF } from "./pdfParser";
// import { convertXMLtoJSON } from "./xmlToJsonConverter";
// import { translateText } from "./translationService";

// export const processData = async () => {
//   const laws = await Law.find();
//   const judgments = await Judgment.find();

//   for (const law of laws) {
//     if (!law.translated) {
//       law.content = await translateText(law.content);
//       law.translated = true;
//       await law.save();
//     }
//   }

//   for (const judgment of judgments) {
//     if (!judgment.translated) {
//       judgment.content = await translateText(judgment.content);
//       judgment.translated = true;
//       await judgment.save();
//     }
//   }
// };
