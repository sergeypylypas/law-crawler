import { parseStringPromise } from "xml2js";

export const convertXMLtoJSON = async (xml: string) => {
  const result = await parseStringPromise(xml);
  return result;
};
