import { Document } from "mongoose";

export enum Languages {
  de = "de",
  fr = "fr",
  it = "it",
  en = "en",
  rm = "rm",
}

export interface ILanguageContent {
  abstract: string;
  rawContent: string;
  processedContent: string;
  url: string;
  translation: number;
  vectorize: boolean;
}

export interface IReference {
  reference: string;
  url: string;
}

export interface IVersion {
  version_id: string;
  is_active: boolean;
  languages: {
    [Languages.de]: ILanguageContent;
    [Languages.fr]: ILanguageContent;
    [Languages.it]: ILanguageContent;
    [Languages.en]: ILanguageContent;
    [Languages.rm]: ILanguageContent;
  };
  date_of_enactment: Date;
}

export interface ILaw extends Document {
  law_id: string;
  title: string;
  jurisdiction: string;
  date_of_termination: Date;
  versions: Array<IVersion>;
  metadata: {
    source_url: string;
    last_checked: Date;
    last_updated: Date;
    abbreviation: string;
    decision_date: Date;
    effective_date: Date;
    source: string;
    languages_of_publication: Array<Languages>;
  };
}

export interface IJudgment extends Document {
  case_id: string;
  caseNumber: string;
  jurisdiction: string;
  languages: {
    [Languages.de]: ILanguageContent;
    [Languages.fr]: ILanguageContent;
    [Languages.it]: ILanguageContent;
    [Languages.en]: ILanguageContent;
  };
  date_of_judgment: Date;
  court: string;
  references: {
    bge: IReference[];
    articles: string[];
  };
  last_update: Date;
  category: string;
}
