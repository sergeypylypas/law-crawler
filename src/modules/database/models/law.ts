import { Schema, model } from "mongoose";
import { ILaw } from "./types";

export const LanguageContentSchema = new Schema({
  abstract: { type: String, required: true },
  rawContent: { type: String, required: true },
  processedContent: { type: String, required: true },
  url: { type: String, required: true },
  translation: { type: Number, required: true },
  vectorize: { type: Boolean, required: true },
});

const VersionSchema = new Schema({
  version_id: { type: String, required: true },
  is_active: { type: Boolean, required: true },
  languages: {
    de: { type: LanguageContentSchema, required: true },
    fr: { type: LanguageContentSchema, required: true },
    it: { type: LanguageContentSchema, required: true },
    en: { type: LanguageContentSchema, required: true },
    rm: { type: LanguageContentSchema, required: true },
  },
  date_of_enactment: { type: Date, required: true },
});

const LawSchema = new Schema({
  law_id: { type: String, required: true },
  title: { type: String, required: true },
  jurisdiction: { type: String, required: true },
  date_of_termination: { type: Date, required: true },
  versions: [VersionSchema],
  metadata: {
    source_url: { type: String, required: true },
    last_checked: { type: Date, required: true },
    last_updated: { type: Date, required: true },
    abbreviation: { type: String, required: true },
    decision_date: { type: Date, required: true },
    effective_date: { type: Date, required: true },
    source: { type: String, required: true },
    languages_of_publication: [
      {
        type: String,
        enum: ["de", "fr", "it", "en", "rm"],
        required: true,
      },
    ],
  },
});

export default model<ILaw>("Law", LawSchema);
