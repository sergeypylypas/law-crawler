import { Schema, model } from "mongoose";
import { IJudgment } from "./types";
import { LanguageContentSchema } from "./law";

const ReferenceSchema = new Schema({
  reference: { type: String, required: true },
  url: { type: String, required: true },
});

const JudgmentSchema = new Schema({
  case_id: { type: String, required: true },
  caseNumber: { type: String, required: true },
  jurisdiction: { type: String, required: true },
  languages: {
    de: { type: LanguageContentSchema, required: true },
    fr: { type: LanguageContentSchema, required: true },
    it: { type: LanguageContentSchema, required: true },
    en: { type: LanguageContentSchema, required: true },
  },
  date_of_judgment: { type: Date, required: true },
  court: { type: String, required: true },
  references: {
    bge: { type: [ReferenceSchema], required: true },
    articles: { type: [String], required: true },
  },
  last_update: { type: Date, required: true },
  category: { type: String, required: true },
});

export default model<IJudgment>("Judgment", JudgmentSchema);
