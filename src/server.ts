import express from "express";
import { connectDB } from "./modules/database/db";
import jobQueue from "./modules/crawler/jobManager";

const app = express();
const PORT = process.env.PORT || 3000;

// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Connect to the database
connectDB();

// Start job manager
jobQueue.start();

// Start server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
